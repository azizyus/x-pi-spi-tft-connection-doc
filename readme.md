## DOC


### CONNECTIONS

the connection list in connection.jpeg is right but i didnt connect my LED but still working (probably just background light)

as you can see the connection.jpeg says MISO but you wont connect that one because probably its for SD CARD, instead of that one you should use SDA like here (https://gitlab.com/azizyus/kmr-1.8-screen-scheme-arduino-uno/blob/master/1.8TFT-Display-V1.0-Pin-Out.png), also its already provided in connections.jpeg  MISO(SDA) but still i want to give example for my self future



### INSTALLATION



this thing works on only ST7735 no idea about other screens

use 3.4, kernel 4.* didnt work

fbtft-modprobe.conf  >>>>  /etc/modprobe.d/fbtft.conf

fbtft-modules-load.conf >>>> /etc/modules-load.d/fbtft.conf

and checkout cat /proc/fb this can give you hint about which buffer you should use while;

con2fbmap  _TTY_NUMBER_  _FBX_

for tty1 and fb8

con2fbmap 1 8



### USEFUL LINKS

https://github.com/nbah22/tiny-font <br>
http://www.orangepi.cn/orangepibbsen/archiver/?tid-3773.html <br>
https://www.zap.org.au/software/fonts/console-fonts-distributed/psftx-opensuse-15.0/index.html <br>
https://www.instructables.com/id/Orange-Pi-Zero-Connect-TFT-SPI-ST7735/ <br>